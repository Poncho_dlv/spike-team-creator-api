{
  "DEBUG": ,
  "SECRET_KEY":"",
  "SESSION_PROTECTION": "",
  "REMEMBER_COOKIE_DURATION": ,
  "REMEMBER_COOKIE_SECURE": ,
  "REMEMBER_COOKIE_HTTPONLY": ,

  "WTF_CSRF_SECRET_KEY": "",
  "WTF_CSRF_ENABLED": ,

  "OAUTH2_CLIENT_ID":,
  "OAUTH2_CLIENT_SECRET":"",
  "OAUTH2_REDIRECT_URI":"",
  "SESSION_TYPE": "",
  "API_BASE_URL": "",

  "SQLALCHEMY_DATABASE_URI": "",
  "SQLALCHEMY_TRACK_MODIFICATIONS": ,
  
  "cyanide_key": "",
  
  "mongo":{
    "host":"",
    "user":"",
    "password":""
  },
  "twitch": {
    "client_id": "",
    "token": ""
  }
}