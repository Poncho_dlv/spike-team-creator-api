#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tc_databases.BaseMongoDb import BaseMongoDb
from bson.objectid import ObjectId
from typing import List


class TCPlayers(BaseMongoDb):
    def add_player(self, player_data: dict):
        self.open_database()
        collection = self.db["players"]
        insert_id = collection.insert_one(player_data).inserted_id
        self.close_database()
        return insert_id

    def remove_player(self, player_id: str):
        self.open_database()
        collection = self.db["players"]
        collection.delete_one({'_id': ObjectId(player_id)})
        self.close_database()

    def get_players(self, players_id: List[ObjectId]):
        self.open_database()
        players = []
        collection = self.db["players"]
        query = {"_id": {"$in": players_id}}
        ret = collection.find(query)
        self.close_database()
        for x in ret:
            players.append(x)
        return players
