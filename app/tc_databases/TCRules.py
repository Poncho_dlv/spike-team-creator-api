#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tc_databases.BaseMongoDb import BaseMongoDb
from bson.objectid import ObjectId


class TCRules(BaseMongoDb):
    def add_rule(self, rule_data: dict):
        self.open_database()
        collection = self.db["rules"]
        insert_id = collection.insert_one(rule_data).inserted_id
        self.close_database()
        return insert_id

    def remove_rule(self, rule_id: str):
        self.open_database()
        collection = self.db["rules"]
        collection.delete_one({'_id': ObjectId(rule_id)})
        self.close_database()

    def get_rules(self):
        self.open_database()
        rules = []
        collection = self.db["rules"]
        ret = collection.find({}, {"_id": 1, "name": 1, "info": 1})
        self.close_database()
        for x in ret:
            rules.append(x)
        return rules

    def get_rule(self, rule_id: str):
        self.open_database()
        collection = self.db["rules"]
        rule = collection.find_one({'_id': ObjectId(rule_id)})
        self.close_database()
        return rule
