#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pymongo
from Settings import Settings


class BaseMongoDb:

    def __init__(self):
        self.client = None
        self.db = None

    def open_database(self):
        if self.client is None or self.db is None:
            db_user = Settings.get_database_user()
            db_password = Settings.get_database_password()
            db_host = Settings.get_database_host()
            if db_host is not None:
                if db_user is not None and db_password is not None:
                    self.client = pymongo.MongoClient("mongodb://{}:{}@{}/TC_DB".format(db_user, db_password, db_host), connect=False)
                else:
                    self.client = pymongo.MongoClient("mongodb://{}/TC_DB".format(db_host), connect=False)
                self.db = self.client["TC_DB"]

    def close_database(self):
        if self.client is not None:
            self.client.close()
            self.client = None
            self.db = None

