#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tc_databases.BaseMongoDb import BaseMongoDb
from bson.objectid import ObjectId
from typing import List


class TCSkills(BaseMongoDb):
    def add_skill(self, skill_data: dict):
        self.open_database()
        collection = self.db["skills"]
        insert_id = collection.insert_one(skill_data).inserted_id
        self.close_database()
        return insert_id

    def remove_skill(self, skill_id: str):
        self.open_database()
        collection = self.db["skills"]
        collection.delete_one({'_id': ObjectId(skill_id)})
        self.close_database()

    def get_skills(self, skills_ids: List[ObjectId]):
        self.open_database()
        skills = []
        collection = self.db["skills"]
        query = {"_id": {"$in": skills_ids}}
        ret = collection.find(query)
        self.close_database()
        for x in ret:
            skills.append(x)
        return skills
