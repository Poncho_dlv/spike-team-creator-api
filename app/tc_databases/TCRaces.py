#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tc_databases.BaseMongoDb import BaseMongoDb
from bson.objectid import ObjectId
from typing import List


class TCRaces(BaseMongoDb):
    def add_race(self, race_data: dict):
        self.open_database()
        collection = self.db["races"]
        insert_id = collection.insert_one(race_data).inserted_id
        self.close_database()
        return insert_id

    def remove_race(self, race_id: str):
        self.open_database()
        collection = self.db["races"]
        collection.delete_one({'_id': ObjectId(race_id)})
        self.close_database()

    def get_races(self, races_id: List[ObjectId]):
        self.open_database()
        races = []
        collection = self.db["races"]
        query = {"_id": {"$in": races_id}}
        ret = collection.find(query, {"_id": 1, "name": 1})
        self.close_database()
        for x in ret:
            races.append(x)
        return races

    def get_race(self, race_id: str):
        self.open_database()
        collection = self.db["races"]
        race = collection.find_one({'_id': ObjectId(race_id)})
        self.close_database()
        return race
