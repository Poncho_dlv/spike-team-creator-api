#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tc_databases.BaseMongoDb import BaseMongoDb
from bson.objectid import ObjectId


class TCSavedTeams(BaseMongoDb):
    def add_team(self, team_data: dict):
        self.open_database()
        collection = self.db["saved_teams"]
        insert_id = collection.insert_one(team_data).inserted_id
        self.close_database()
        return insert_id

    def update_team(self, team_data: dict):
        self.open_database()
        collection = self.db["saved_teams"]
        team_id = team_data.get("_id")
        team_data.pop("_id")
        result = collection.update_one({"_id": ObjectId(team_id)}, {"$set": team_data})
        self.close_database()
        return result

    def remove_team(self, team_id: str):
        self.open_database()
        collection = self.db["saved_teams"]
        collection.delete_one({'_id': ObjectId(team_id)})
        self.close_database()

    def get_teams(self, owners_id: str):
        self.open_database()
        teams = []
        collection = self.db["saved_teams"]
        ret = collection.find({"owner_id": owners_id}, {"_id": 1, "name": 1})
        self.close_database()
        for x in ret:
            teams.append(x)
        return teams

    def get_team(self, team_id: str):
        self.open_database()
        collection = self.db["saved_teams"]
        team = collection.find_one({'_id': ObjectId(team_id)})
        self.close_database()
        return team
