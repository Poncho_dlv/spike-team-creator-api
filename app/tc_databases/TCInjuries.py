#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tc_databases.BaseMongoDb import BaseMongoDb
from bson.objectid import ObjectId
from typing import List


class TCInjuries(BaseMongoDb):
    def add_injury(self, injury_data: dict):
        self.open_database()
        collection = self.db["injuries"]
        insert_id = collection.insert_one(injury_data).inserted_id
        self.close_database()
        return insert_id

    def remove_injury(self, injury_id: str):
        self.open_database()
        collection = self.db["injuries"]
        collection.delete_one({'_id': ObjectId(injury_id)})
        self.close_database()

    def get_injuries(self, injuries_id: List[ObjectId]):
        self.open_database()
        injuries = []
        collection = self.db["injuries"]
        query = {"_id": {"$in": injuries_id}}
        ret = collection.find(query)
        self.close_database()
        for x in ret:
            injuries.append(x)
        return injuries
