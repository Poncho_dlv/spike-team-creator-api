#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tc_databases.BaseMongoDb import BaseMongoDb
from bson.objectid import ObjectId
from typing import List


class TCInducements(BaseMongoDb):
    def add_inducement(self, inducement_data: dict):
        self.open_database()
        collection = self.db["inducements"]
        insert_id = collection.insert_one(inducement_data).inserted_id
        self.close_database()
        return insert_id

    def remove_inducement(self, inducement_id: str):
        self.open_database()
        collection = self.db["inducements"]
        collection.delete_one({'_id': ObjectId(inducement_id)})
        self.close_database()

    def get_inducements(self, inducements_id: List[ObjectId]):
        self.open_database()
        inducements = []
        collection = self.db["inducements"]
        query = {"_id": {"$in": inducements_id}}
        ret = collection.find(query)
        self.close_database()
        for x in ret:
            inducements.append(x)
        return inducements
