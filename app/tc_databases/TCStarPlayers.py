#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tc_databases.BaseMongoDb import BaseMongoDb
from bson.objectid import ObjectId
from typing import List


class TCStarPlayers(BaseMongoDb):
    def add_star_player(self, star_player_data: dict):
        self.open_database()
        collection = self.db["star_players"]
        insert_id = collection.insert_one(star_player_data).inserted_id
        self.close_database()
        return insert_id

    def remove_star_player(self, star_player_id: str):
        self.open_database()
        collection = self.db["star_players"]
        collection.delete_one({'_id': ObjectId(star_player_id)})
        self.close_database()

    def get_star_players(self, star_players_ids: List[ObjectId]):
        self.open_database()
        star_players = []
        collection = self.db["star_players"]
        query = {"_id": {"$in": star_players_ids}}
        ret = collection.find(query)
        self.close_database()
        for x in ret:
            star_players.append(x)
        return star_players
