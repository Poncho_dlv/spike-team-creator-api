from pydantic import BaseModel, Field
from typing import List


class PlayerModel(BaseModel):
    player_id: str
    added_skills: List[str] = []
    name: str


class SaveTeamModel(BaseModel):
    rule_id: str
    race_id: str
    name: str
    description: str = None
    owner_id: str
    team_value: int
    apothecary: int
    assistant_coaches: int
    cheerleaders: int
    fan_factor: int
    rerolls: int
    inducements: List[str] = []
    star_player: List[str] = []
    roster: List[PlayerModel]


class UpdateTeamModel(SaveTeamModel):
    team_id: str = Field(..., alias="_id")
