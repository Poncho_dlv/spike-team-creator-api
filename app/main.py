import uvicorn
from bson.errors import InvalidId
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware

from Models import SaveTeamModel, UpdateTeamModel
from spike_user_db.Users import Users
from tc_databases.TCInducements import TCInducements
from tc_databases.TCInjuries import TCInjuries
from tc_databases.TCPlayers import TCPlayers
from tc_databases.TCRaces import TCRaces
from tc_databases.TCRules import TCRules
from tc_databases.TCSavedTeams import TCSavedTeams
from tc_databases.TCSkills import TCSkills
from tc_databases.TCStarPlayers import TCStarPlayers

app = FastAPI()


origins = [
    "https://team-creator.spike.ovh",
    "https://www.team-creator.spike.ovh"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/rules")
def get_rules():
    rules_db = TCRules()
    rules = rules_db.get_rules()
    for rule in rules:
        rule["_id"] = str(rule["_id"])
    return {"rules": rules}


@app.get("/rules/{rule_id}")
def get_rule(rule_id: str):
    rules_db = TCRules()
    skills_db = TCSkills()
    injuries_db = TCInjuries()

    try:
        rule = rules_db.get_rule(rule_id)
    except InvalidId:
        raise HTTPException(status_code=404, detail="Invalid id")

    if rule is None:
        raise HTTPException(status_code=404, detail="Rule not found")

    rule.pop("races", None)
    rule["_id"] = str(rule["_id"])

    skills = skills_db.get_skills(rule["skills"])
    split_skills = {
        "general": [],
        "agility": [],
        "passing": [],
        "strength": [],
        "mutation": [],
        "stats_increase": [],
        "extraordinary": []
    }
    for skill in skills:
        skill["_id"] = str(skill["_id"])
        split_skills[skill["type"]].append(skill)
    rule["skills"] = split_skills

    injuries = injuries_db.get_injuries(rule["injuries"])
    for injury in injuries:
        injury["_id"] = str(injury["_id"])
    rule["injuries"] = injuries
    return rule


@app.get("/rules/{rule_id}/races")
def get_races(rule_id: str):
    rules_db = TCRules()
    races_db = TCRaces()

    try:
        rule = rules_db.get_rule(rule_id)
    except InvalidId:
        raise HTTPException(status_code=404, detail="Invalid id")

    if rule is None:
        raise HTTPException(status_code=404, detail="Races not found")

    races = races_db.get_races(rule["races"])
    for race in races:
        race["_id"] = str(race["_id"])
    return races


@app.get("/races/{race_id}")
def get_race(race_id: str):
    races_db = TCRaces()
    star_player_db = TCStarPlayers()
    players_db = TCPlayers()
    inducements_db = TCInducements()
    skills_db = TCSkills()

    try:
        race = races_db.get_race(race_id)
    except InvalidId:
        raise HTTPException(status_code=404, detail="Invalid id")

    if race is None:
        raise HTTPException(status_code=404, detail="Race not found")

    race["_id"] = str(race["_id"])
    star_players = star_player_db.get_star_players(race["star_players"])
    for star_player in star_players:
        star_player["_id"] = str(star_player["_id"])
        skills = skills_db.get_skills(star_player["starting_skills"])
        for skill in skills:
            skill["_id"] = str(skill["_id"])
        star_player["starting_skills"] = skills

    race["star_players"] = star_players

    players = players_db.get_players(race["roster"])
    for player in players:
        player["_id"] = str(player["_id"])
        skills = skills_db.get_skills(player["starting_skills"])
        for skill in skills:
            skill["_id"] = str(skill["_id"])
        player["starting_skills"] = skills
    race["roster"] = players

    inducements = inducements_db.get_inducements(race["inducements"])
    for inducement in inducements:
        inducement["_id"] = str(inducement["_id"])
    race["inducements"] = inducements

    return race


@app.post("/save_team")
async def save_team(team: SaveTeamModel):
    user_db = Users()
    if user_db.user_exist(team.owner_id):
        save_db = TCSavedTeams()
        team_data = team.dict(exclude_none=True, by_alias=True)
        inserted_id = save_db.add_team(team_data)
        if inserted_id is not None:
            result_json = {
                "action": "add",
                "result": "success",
                "team_id": str(inserted_id)
            }
        else:
            result_json = {
                "action": "add",
                "result": "error"
            }
        return result_json
    else:
        raise HTTPException(status_code=403, detail="Invalid user")


@app.put("/update_team")
async def update_team(team: UpdateTeamModel):
    save_db = TCSavedTeams()
    team_data = team.dict(exclude_none=True, by_alias=True)
    team_id = team_data.get("_id")

    result = save_db.update_team(team_data)
    if result.matched_count == 0:
        result_json = {
            "action": "update",
            "result": "error",
            "details": "Team not found",
            "team_id": str(team_id)
        }
    elif result.matched_count == 1 and result.modified_count == 0:
        result_json = {
            "action": "update",
            "result": "success",
            "details": "No change",
            "team_id": str(team_id)
        }
    else:
        result_json = {
            "action": "update",
            "result": "success",
            "team_id": str(team_id)
        }
    return result_json


@app.get("/teams/{team_id}")
async def get_team(team_id: str):
    save_db = TCSavedTeams()

    try:
        team_data = save_db.get_team(team_id)
        team_data["_id"] = str(team_data["_id"])
    except:
        team_data = {
            "action": "get_team",
            "result": "error",
            "team_id": team_id
        }
    return team_data


@app.delete("/teams/{team_id}/delete")
async def delete_team(team_id: str):
    save_db = TCSavedTeams()
    save_db.remove_team(team_id)
    return {"action": "delete", "result": "success", "team_id": str(team_id)}


@app.get("/users/{user_id}/teams")
async def get_user_teams(owner_id: str):
    save_db = TCSavedTeams()

    try:
        team_list = save_db.get_teams(owner_id)
        for team in team_list:
            team["_id"] = str(team["_id"])
    except:
        return {
            "action": "get_teams",
            "result": "error",
            "owner_id": owner_id
        }
    return team_list

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
