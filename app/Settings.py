import json
import os


class Settings:

    @staticmethod
    def load_json():
        filename = os.environ.get("SPIKE_SETTINGS_FILE", "settings/spike_settings.json")
        try:
            with open(filename, encoding="utf-8") as f:
                json_data = json.load(f)
        except FileNotFoundError:
            json_data = {}
        return json_data

    # Mongo Db
    @staticmethod
    def get_database_host():
        json_data = Settings.load_json()
        return json_data.get("TC_MONGO_DB", {}).get("HOST")

    @staticmethod
    def get_database_user():
        json_data = Settings.load_json()
        return json_data.get("TC_MONGO_DB", {}).get("USER")

    @staticmethod
    def get_database_password():
        json_data = Settings.load_json()
        return json_data.get("TC_MONGO_DB", {}).get("PASSWORD")


